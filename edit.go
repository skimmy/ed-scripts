// Copyright 2020 Michele Schimd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"strings"
)

type CellIndex struct {
	i, j, delta uint64
}

type Script []CellIndex

func (s Script) Cost() (d uint64) {
	for i := range s {
		d += s[i].delta
	}
	return d
}

func (s Script) Match() (m uint64) {
	for i := 0; i < len(s)-1; i++ {
		if s[i].delta == 0 && s[i].j-s[i+1].j == 1 && s[i].i-s[i+1].i == 1 {
			m++
		}
	}
	return m
}

func (s Script) Substitutions() (sub uint64) {
	for i := 0; i < len(s)-1; i++ {
		if s[i].delta == 1 && s[i].j-s[i+1].j == 1 && s[i].i-s[i+1].i == 1 {
			sub++
		}
	}
	return sub
}

func (s Script) Insertions() (ins uint64) {
	for i := 0; i < len(s)-1; i++ {
		if s[i].j-s[i+1].j == 0 && s[i].i-s[i+1].i == 1 {
			ins++
		}
	}
	return ins
}

func (s Script) Deletions() (del uint64) {
	for i := 0; i < len(s)-1; i++ {
		if s[i].j-s[i+1].j == 1 && s[i].i-s[i+1].i == 0 {
			del++
		}
	}
	return del
}

func scriptFor(x, y string, btAlg int) Script {
	if btAlg == BTAlgorithmClose {
		dpMatrix := makeUin64Matrix(len(x)+1, len(y)+1)
		_ = editDistance(x, y, dpMatrix)
		return closeDiagonalBacktracking(dpMatrix, uint64(len(x)), uint64(len(y)))
	}
	if btAlg == BTAlgorithmMinSpread {
		dpMatrix := editDistanceWithInfo(x, y)
		return minSpreadBacktracking(dpMatrix, uint64(len(x)), uint64(len(y)))
	}
	return nil
}

func dpMatrixInit(m [][]uint64) {
	for r := range m {
		m[r][0] = uint64(r)
	}
	for c := range m[0] {
		m[0][c] = uint64(c)
	}
}

func editDistance(x, y string, dpMatrix [][]uint64) uint64 {
	n := len(x)
	m := len(y)
	if dpMatrix == nil {
		dpMatrix = make([][]uint64, n+1)
		for r := range dpMatrix {
			dpMatrix[r] = make([]uint64, m+1)
		}
	}
	dpMatrixInit(dpMatrix)
	for i := 1; i <= n; i++ {
		for j := 1; j <= m; j++ {
			var delta uint64
			if x[i-1] != y[j-1] {
				delta = 1
			}
			dpMatrix[i][j] = Min(
				dpMatrix[i-1][j-1]+delta,
				Min(dpMatrix[i-1][j]+1, dpMatrix[i][j-1]+1))
		}
	}
	return dpMatrix[n][m]
}

func closeDiagonalBacktracking(dpMatrix [][]uint64, n, m uint64) Script {
	i, j := n, m
	script := make([]CellIndex, 0, n+m)
	for i > 0 && j > 0 {
		a, b, c, d := dpMatrix[i-1][j-1], dpMatrix[i-1][j], dpMatrix[i][j-1], dpMatrix[i][j]
		if a == d && a <= b && a <= c { // Match
			script = append(script, CellIndex{i, j, 0})
			i--
			j--
			continue
		}
		if a < b && a < c { // Sub
			script = append(script, CellIndex{i, j, 1})
			i--
			j--
			continue
		}
		if b < a && b < c { // Del
			script = append(script, CellIndex{i, j, 1})
			i--
			continue
		}
		if c < a && c < b { // Del
			script = append(script, CellIndex{i, j, 1})
			j--
			continue
		}
		if a == b && b == c { // Tie All
			script = append(script, CellIndex{i, j, 1})
			if i < j {
				j--
			}
			if i == j {
				i--
				j--
			}
			if i > j {
				i--
			}
			continue
		}
		script = append(script, CellIndex{i, j, 1})
		if a == b {
			if i > j {
				i--
			} else {
				i--
				j--
			}
		}
		if a == c {
			if i < j {
				j--
			} else {
				i--
				j--
			}
		}
		if b == c {
			if i < j {
				j--
			} else {
				i--
			}
		}
	}
	for i > 0 {
		script = append(script, CellIndex{i, j, 1})
		i--
	}
	for j > 0 {
		script = append(script, CellIndex{i, j, 1})
		j--
	}
	script = append(script, CellIndex{0, 0, 0})
	return script
}

func (s Script) ApplyTo(x, y string) (string, string, string) {
	xBuild := strings.Builder{}
	yBuild := strings.Builder{}
	sBuild := strings.Builder{}
	xBuild.Grow(len(x) + len(y))
	yBuild.Grow(len(x) + len(y))
	sBuild.Grow(len(x) + len(y))

	for k := len(s) - 1; k > 0; k-- {
		var cx, cy, cs uint8
		sk, sk1 := s[k], s[k-1]
		if sk1.i-sk.i == 1 && sk1.j-sk.j == 1 { // Diagonal
			if sk1.delta == 1 { // SUB
				cx = x[sk1.i-1]
				cy = y[sk1.j-1]
				cs = 'S'
			} else { // MATCH
				cx = x[sk1.i-1]
				cy = y[sk1.j-1]
				cs = 'M'
			}
		}
		if sk1.i-sk.i == 0 && sk1.j-sk.j == 1 { // INS
			cx = '-'
			cy = y[sk1.j-1]
			cs = 'I'
		}
		if sk1.i-sk.i == 1 && sk1.j-sk.j == 0 { // DEL
			cx = x[sk1.i-1]
			cy = '-'
			cs = 'D'
		}
		_, _ = fmt.Fprintf(&sBuild, "%c", cs)
		_, _ = fmt.Fprintf(&xBuild, "%c", cx)
		_, _ = fmt.Fprintf(&yBuild, "%c", cy)
	}
	return xBuild.String(), yBuild.String(), sBuild.String()
}

type EDInfo struct {
	edit      uint64
	minSpread uint64
}

func (d *EDInfo) UpdateEdInfo(a, b, c EDInfo, delta uint64, i, j int) *EDInfo {
	d.edit = Min(Min(a.edit+delta, b.edit+1), c.edit+1)
	if d.edit == a.edit && delta == 0 { // Match
		d.minSpread = a.minSpread
		if b.edit == d.edit-1 {
			d.minSpread = Min(d.minSpread, b.minSpread)
		}
		if c.edit == d.edit-1 {
			d.minSpread = Min(d.minSpread, c.minSpread)
		}
	} else {
		min := Min(a.edit, Min(b.edit, c.edit))
		if a.edit == min {
			d.minSpread = a.minSpread
			if b.edit == min {
				d.minSpread = Min(d.minSpread, b.minSpread)
			}
			if c.edit == min {
				d.minSpread = Min(d.minSpread, c.minSpread)
			}
		} else {
			if b.edit == c.edit {
				d.minSpread = Min(b.minSpread, c.minSpread)
			} else {
				if b.edit < c.edit {
					d.minSpread = b.minSpread
				} else {
					d.minSpread = c.minSpread
				}
			}
		}
	}
	var ij uint64 = 0
	if i > j {
		ij = uint64(i - j)
	} else {
		ij = uint64(j - i)
	}
	d.minSpread = Max(d.minSpread, ij)
	return d
}

func editDistanceWithInfo(x, y string) [][]EDInfo {
	n := len(x)
	m := len(y)
	matrix := make([][]EDInfo, n+1)
	for i := range matrix {
		matrix[i] = make([]EDInfo, m+1)
	}
	for i := 0; i <= n; i++ {
		matrix[i][0].edit = uint64(i)
		matrix[i][0].minSpread = uint64(i)
	}
	for j := 0; j <= n; j++ {
		matrix[0][j].edit = uint64(j)
		matrix[0][j].minSpread = uint64(j)
	}
	for i := 1; i <= n; i++ {
		for j := 1; j <= m; j++ {
			var delta uint64 = 0
			if x[i-1] != y[j-1] {
				delta = 1
			}
			matrix[i][j].UpdateEdInfo(
				matrix[i-1][j-1], matrix[i-1][j], matrix[i][j-1], delta, i, j)
		}
	}
	return matrix
}

func minSpreadBacktracking(dpMatrix [][]EDInfo, n, m uint64) Script {
	i, j := n, m
	script := make([]CellIndex, 0, n+m)
	script = append(script, CellIndex{i, j, 0})
	for i > 0 && j > 0 {
		a, b, c, d := dpMatrix[i-1][j-1], dpMatrix[i-1][j], dpMatrix[i][j-1], dpMatrix[i][j]
		if a.edit == d.edit && a.edit <= b.edit && a.edit <= c.edit { // Match
			script = append(script, CellIndex{i, j, 0})
			i--
			j--
			continue
		}
		if a.edit < b.edit && a.edit < c.edit { // Sub
			script = append(script, CellIndex{i, j, 1})
			i--
			j--
			continue
		}
		if b.edit < a.edit && b.edit < c.edit { // Del
			script = append(script, CellIndex{i, j, 1})
			i--
			continue
		}
		if c.edit < a.edit && c.edit < b.edit { // Ins
			script = append(script, CellIndex{i, j, 1})
			j--
			continue
		}
		if a.edit == b.edit && b.edit == c.edit { // Tie All
			script = append(script, CellIndex{i, j, 1})
			if a.minSpread <= b.minSpread && a.minSpread <= c.minSpread {
				i--
				j--
			} else {
				if c.minSpread < a.minSpread && c.minSpread < b.minSpread {
					j--
				}

				if b.minSpread < a.minSpread && b.minSpread < c.minSpread {
					i--
				}
			}
			continue
		}
		script = append(script, CellIndex{i, j, 1})
		if a.edit == b.edit {
			if b.minSpread < a.minSpread {
				i--
			} else {
				i--
				j--
			}
		}
		if a.edit == c.edit {
			if c.minSpread < a.minSpread {
				j--
			} else {
				i--
				j--
			}
		}
		if b.edit == c.edit {
			if b.minSpread < c.minSpread {
				j--
			} else {
				i--
			}
		}
	}
	for i > 0 {
		script = append(script, CellIndex{i, j, 1})
		i--
	}
	for j > 0 {
		script = append(script, CellIndex{i, j, 1})
		j--
	}
	script = append(script, CellIndex{0, 0, 0})
	return script
}
