// Copyright 2020 Michele Schimd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"math/rand"
	"time"
)

var RandGen = rand.New(rand.NewSource(time.Now().UnixNano()))

func tests() string {
	x, y := "TACGT", "ACGTG"
	n, m := len(x), len(y)
	dpMatrix := make([][]uint64, n+1)
	for r := range dpMatrix {
		dpMatrix[r] = make([]uint64, m+1)
	}
	dpMatrixInit(dpMatrix)
	_ = editDistance(x, y, dpMatrix)
	script := closeDiagonalBacktracking(dpMatrix, uint64(n), uint64(m))
	xs, ys, s := script.ApplyTo(x, y)
	return fmt.Sprintf("%s\n%s\n%s\n", xs, ys, s)
}

func main() {
	consoleLoop()
	//x := "ACTCTACTCT"
	//y := "CTCTCTCTGG"
	//m := editDistanceWithInfo(x, y)
	//sc := minSpreadBacktracking(m, uint64(len(x)), uint64(len(y)))
	//xt, yt, s := sc.ApplyTo(x,y)
	//fmt.Printf("%s\n%s\n%s\n", s, xt, yt)
}
