Edit Scripts
============
This software allows manipulation of *edit scripts* from the command line console.
It can be used to compute edit scripts and apply them to strings. Results
can be saved to file for later re-use.

To-Do
-----
The following are desirable features of the software. Strike-out features
are already implemented.

1. ~~Compute edit distance~~
2. ~~Compute edit scripts~~
3. ~~Apply script to strings~~
4. Save output  
5. Load from saved file
6. Terminal interface
   - ~~Type input strings~~
   - ~~Random generate input strings~~
   - Save results to file
7. Extend string with more character
8. Select range within the 'current' strings
9. Full information algorithm
   - Min spread script