// Copyright 2020 Michele Schimd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import "fmt"

const InitialHistoryCapacity int = 256

const BTAlgorithmClose int = 0
const BTAlgorithmMinSpread int = 1

var BTAlgs = []string{
	"Closest To Diagonal Heuristic",
	"Minimum spread",
}

type Environment struct {
	CommandHistory []string
	OutputHistory  []string
	x              string
	y              string
	n              int
	m              int
	BTAlg          int
}

var Env = Environment{
	CommandHistory: make([]string, 0, InitialHistoryCapacity),
	OutputHistory:  make([]string, 0, InitialHistoryCapacity),
	n:              16,
	m:              16,
	BTAlg:          BTAlgorithmMinSpread,
}

func printEnvironment() string {
	return fmt.Sprintf("n: %d\nm: %d\nx: %s\ny: %s\nBT: %s", Env.n, Env.m, Env.x, Env.y, BTAlgs[Env.BTAlg])
}
