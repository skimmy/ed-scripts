// Copyright 2020 Michele Schimd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"text/tabwriter"
)

// TODO: the range map returns random order this may not be good for help messages
var helpStrings = []string{
	"   edit    Compute edit distance for given strings\n",
	"   size    Changes the current size\n",
	"   mat     Computes and prints the DP matrix for x and y\n",
	"   env     Prints the current environment\n",
	"   help,h  Show this help message\n",
	"   quit,h  Quit the program\n",
}

func helpString() string {
	builder := strings.Builder{}
	fmt.Fprintf(&builder, "\nCommands:\n")
	for _, element := range helpStrings {
		fmt.Fprintf(&builder, element)
	}
	return builder.String()
}

func readStrings(scanner *bufio.Scanner) (command string) {
	fmt.Print("\tx -> ")
	scanner.Scan()
	Env.x = scanner.Text()
	command = command + "\n" + Env.x
	if len(Env.x) > 0 && Env.x[0] == '#' {
		Env.x = randomString(Env.n, RandGen)
		fmt.Printf("\tx -> %s\n", Env.x)
	}
	fmt.Print("\ty -> ")
	scanner.Scan()
	Env.y = scanner.Text()
	command = command + "\n" + Env.y
	if len(Env.y) > 0 && Env.y[0] == '#' {
		Env.y = randomString(Env.m, RandGen)
		fmt.Printf("\ty -> %s\n", Env.y)
	}
	return command
}

func consoleLoop() {
	currentLine := 0
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Printf("[%d]: ", currentLine)
	for scanner.Scan() {
		text := scanner.Text()
		command := text
		if text == "q" || text == "quit" {
			Env.CommandHistory = append(Env.CommandHistory, text)
			return
		}
		output := ""
		if text == "h" || text == "help" {
			output = helpString()
		}
		if text == "env" {
			output = printEnvironment()
		}
		if text == "test" {
			output = tests()
		}
		if text == "edit" {
			command = command + readStrings(scanner)
			script := scriptFor(Env.x, Env.y, Env.BTAlg)
			xs, ys, s := script.ApplyTo(Env.x, Env.y)
			output = fmt.Sprintf(
				"\n\tED %d (M %d, S %d, D %d, I %d)\n\t%s\n\t%s\n\t%s",
				script.Cost(), script.Match(), script.Substitutions(),
				script.Deletions(), script.Insertions(), s, xs, ys)
		}
		if text == "size" {
			var t string
			fmt.Print("\tn -> ")
			scanner.Scan()
			t = scanner.Text()
			command = command + "\n" + t
			n, err := strconv.Atoi(t)
			if err != nil {
				output = fmt.Sprintf("Error %s is not a valid size", t)
			} else {
				Env.n = n
			}
			fmt.Print("\tm -> ")
			scanner.Scan()
			t = scanner.Text()
			command = command + "\n" + t
			n, err = strconv.Atoi(t)
			if err != nil {
				output = fmt.Sprintf("Error %s is not a valid size", t)
			} else {
				Env.m = n
			}
		}
		if text == "bt" {
			var t string
			fmt.Print("\tAlg -> ")
			scanner.Scan()
			t = scanner.Text()
			command = command + "\n" + t
			n, err := strconv.Atoi(t)
			if err != nil || (n < 0 && n > 1) {
				output = fmt.Sprintf("Error %s is not a valid BT algorithm (0..1)", t)
			} else {
				Env.BTAlg = n
			}
		}
		if text == "mat" {
			dpMatrixInfo := editDistanceWithInfo(Env.x, Env.y)
			tw := new(tabwriter.Writer)
			tw.Init(os.Stdout, 3, 5, 0, ' ', 0)
			fmt.Fprintf(tw, " \t \t")
			for j := 0; j < len(Env.y); j++ {
				fmt.Fprintf(tw, "%c\t", Env.y[j])
			}
			fmt.Fprintln(tw)
			for i := 0; i <= len(Env.x); i++ {
				if i > 0 {
					fmt.Fprintf(tw, "%c\t", Env.x[i-1])
				} else {
					fmt.Fprintf(tw, "\t")
				}
				for j := 0; j <= len(Env.y); j++ {
					fmt.Fprintf(tw, "%d\t", dpMatrixInfo[i][j].edit)
				}
				fmt.Fprintln(tw)
				tw.Flush()
			}
		}
		Env.CommandHistory = append(Env.CommandHistory, command)
		Env.OutputHistory = append(Env.OutputHistory, output)
		currentLine++
		fmt.Printf("%s\n[%d]: ", output, currentLine)
	}
}
